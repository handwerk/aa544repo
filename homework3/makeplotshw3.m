%% plot data from homework2
%% clear data
clear all;
close all;
clc;
%% set plot defaults
set(0,'DefaultAxesFontSize',18);
set(0,'DefaultLineLineWidth',2);


% %% coarse grid
% load resCoarse.dat;
% load uvpCoarse.dat;
% UVP = uvpCoarse;
% Nx = 64;
% Ny = 24;
% 
% grid = Nx*Ny;
% frames = length(UVP(:,1))/(grid);
% 
% x = zeros(Nx,Ny,frames);
% y = zeros(Nx,Ny,frames);
% u = zeros(Nx,Ny,frames);
% v = zeros(Nx,Ny,frames);
% p = zeros(Nx,Ny,frames);
% 
% for i = 1:frames
%     % resphape UVP into frames
%     x = UVP(1 + grid*(i-1):grid*i,1);
%     y = UVP(1 + grid*(i-1):grid*i,2);
%     u = UVP(1 + grid*(i-1):grid*i,3);
%     v = UVP(1 + grid*(i-1):grid*i,4);
%     p = UVP(1 + grid*(i-1):grid*i,5);
%     Xcoarse = reshape(x,Nx,Ny);
%     Ycoarse = reshape(y,Nx,Ny);
%     Ucoarse = reshape(u,Nx,Ny);
%     Vcoarse = reshape(v,Nx,Ny);
%     Pcoarse = reshape(p,Nx,Ny);
%     
% end
% %% medium grid
% load resMed.dat;
% load uvpMed.dat;
% UVP = uvpMed;
% Nx = 128;
% Ny = 48;
% 
% grid = Nx*Ny;
% frames = length(UVP(:,1))/(grid);
% 
% x = zeros(Nx,Ny,frames);
% y = zeros(Nx,Ny,frames);
% u = zeros(Nx,Ny,frames);
% v = zeros(Nx,Ny,frames);
% p = zeros(Nx,Ny,frames);
% 
% for i = 1:frames
%     % resphape UVP into frames
%     x = UVP(1 + grid*(i-1):grid*i,1);
%     y = UVP(1 + grid*(i-1):grid*i,2);
%     u = UVP(1 + grid*(i-1):grid*i,3);
%     v = UVP(1 + grid*(i-1):grid*i,4);
%     p = UVP(1 + grid*(i-1):grid*i,5);
%     Xmed = reshape(x,Nx,Ny);
%     Ymed = reshape(y,Nx,Ny);
%     Umed = reshape(u,Nx,Ny);
%     Vmed = reshape(v,Nx,Ny);
%     Pmed = reshape(p,Nx,Ny);
%     
% end
%% fine grid
load resFine.dat;
load uvpFine.dat;
UVP = uvpFine;
Nx = 96;
Ny = 96;

grid = Nx*Ny;
frames = length(UVP(:,1))/(grid);

x = zeros(Nx,Ny,frames);
y = zeros(Nx,Ny,frames);
u = zeros(Nx,Ny,frames);
v = zeros(Nx,Ny,frames);
p = zeros(Nx,Ny,frames);

for i = 1:frames
    % resphape UVP into frames
    x = UVP(1 + grid*(i-1):grid*i,1);
    y = UVP(1 + grid*(i-1):grid*i,2);
    u = UVP(1 + grid*(i-1):grid*i,3);
    v = UVP(1 + grid*(i-1):grid*i,4);
    p = UVP(1 + grid*(i-1):grid*i,5);
    Xfine = reshape(x,Nx,Ny);
    Yfine = reshape(y,Nx,Ny);
    Ufine = reshape(u,Nx,Ny);
    Vfine = reshape(v,Nx,Ny);
    Pfine = reshape(p,Nx,Ny);
%     pcolor(Xfine,Yfine,Pfine); colorbar; shading interp; hold on
%     drawnow
%     pause(.5)
end


%% steady state plots
figure()
pcolor(Xfine,Yfine,Ufine); colorbar; colormap jet; shading interp; axis square
title('U');
figure()
pcolor(Xfine,Yfine,Vfine); colorbar; colormap jet; shading interp; axis square
title('V');
figure()
pcolor(Xfine,Yfine,Pfine); colorbar; colormap jet; shading interp; axis square
title('Pressure');

%% grid
figure()
contour(Xfine,length(Xfine)); colormap ([0 0 0])
hold on
contour(Yfine,length(Yfine)); colormap ([0 0 0])
title('mesh')

%% quiver/streamline
% figure()
% quiver(Xfine((1:3:end),(1:3:end)),Yfine((1:3:end),(1:3:end))...
%     ,Ufine((1:3:end),(1:3:end)),Vfine((1:3:end),(1:3:end)),2);...
% xlim([0 1]); ylim([0 1]); axis square


[sx1, sy1] = meshgrid(1, 1:2:16); % inlet
[sx2, sy2] = meshgrid(19:3:32, 22:3:25); % start of side branch
[sx3, sy3] = meshgrid([23 35 42] , [2 4]); 

figure()
streamline(Ufine',Vfine',sx1,sy1,[.1,2000]); axis square
hold on
streamline(Ufine',Vfine',sx2,sy2,[.1, 500]);
streamline(Ufine',Vfine',sx3,sy3,[.1, 500]);
title('Streamlines')

%% plot residual vs time
figure()
% semilogy(resFine(:,1),resFine(:,4), resMed(:,1),resMed(:,4), resCoarse(:,1),resCoarse(:,4))
semilogy(resFine(:,1),resFine(:,4));
legend('Fine')
xlabel('time')
ylabel('residual')


%% profile

figure()
subplot(2,4,1)
plot(flipud(Ufine(10,1:16))); view(-90,90);axis ij;title('x=10'); axis square
subplot(2,4,2)
plot(flipud(Ufine(20,1:16))); view(-90,90);axis ij;title('x=20'); axis square
subplot(2,4,3)
plot(flipud(Ufine(30,1:16))); view(-90,90);axis ij;title('x=30'); axis square
subplot(2,4,4)
plot(flipud(Ufine(40,1:16))); view(-90,90);axis ij;title('x=40'); axis square
subplot(2,4,5)
plot(flipud(Ufine(50,1:16))); view(-90,90);axis ij;title('x=50'); axis square
subplot(2,4,6)
plot(flipud(Ufine(60,1:16))); view(-90,90);axis ij;title('x=60'); axis square
subplot(2,4,7)
plot(flipud(Ufine(70,1:16))); view(-90,90);axis ij;title('x=70'); axis square
subplot(2,4,8)
plot(flipud(Ufine(80,1:16))); view(-90,90);axis ij;title('x=80'); axis square

figure()
subplot(2,4,1)
plot(flipud(Vfine(17:32,20))); title('y=20'); axis square
subplot(2,4,2)
plot(flipud(Vfine(17:32,30))); title('y=30'); axis square
subplot(2,4,3)
plot(flipud(Vfine(17:32,40))); title('y=40'); axis square
subplot(2,4,4)
plot(flipud(Vfine(17:32,50))); title('y=50'); axis square
subplot(2,4,5)
plot(flipud(Vfine(17:32,60))); title('y=60'); axis square
subplot(2,4,6)
plot(flipud(Vfine(17:32,70))); title('y=70'); axis square
subplot(2,4,7)
plot(flipud(Vfine(17:32,80))); title('y=80'); axis square
subplot(2,4,8)
plot(flipud(Vfine(17:32,90))); title('y=90'); axis square


