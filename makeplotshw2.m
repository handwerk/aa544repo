%% plot data from homework2
%% clear data
clear all;
close all;
clc;
%% set plot defaults
set(0,'DefaultAxesFontSize',18);
set(0,'DefaultLineLineWidth',2);


%% coarse grid
load residual64x24.dat;
load UVP64x24.dat;
UVP = UVP64x24;
resCoarse = residual64x24;
Nx = 64;
Ny = 24;

grid = Nx*Ny;
frames = length(UVP(:,1))/(grid);

x = zeros(Nx,Ny,frames);
y = zeros(Nx,Ny,frames);
u = zeros(Nx,Ny,frames);
v = zeros(Nx,Ny,frames);
p = zeros(Nx,Ny,frames);

for i = 1:frames
    % resphape UVP into frames
    x = UVP(1 + grid*(i-1):grid*i,1);
    y = UVP(1 + grid*(i-1):grid*i,2);
    u = UVP(1 + grid*(i-1):grid*i,3);
    v = UVP(1 + grid*(i-1):grid*i,4);
    p = UVP(1 + grid*(i-1):grid*i,5);
    Xcoarse = reshape(x,Nx,Ny);
    Ycoarse = reshape(y,Nx,Ny);
    Ucoarse = reshape(u,Nx,Ny);
    Vcoarse = reshape(v,Nx,Ny);
    Pcoarse = reshape(p,Nx,Ny);
    
end
%% medium grid
load residual128x48.dat;
load UVP128x48.dat;
UVP = UVP128x48;
resMed = residual128x48;
Nx = 128;
Ny = 48;

grid = Nx*Ny;
frames = length(UVP(:,1))/(grid);

x = zeros(Nx,Ny,frames);
y = zeros(Nx,Ny,frames);
u = zeros(Nx,Ny,frames);
v = zeros(Nx,Ny,frames);
p = zeros(Nx,Ny,frames);

for i = 1:frames
    % resphape UVP into frames
    x = UVP(1 + grid*(i-1):grid*i,1);
    y = UVP(1 + grid*(i-1):grid*i,2);
    u = UVP(1 + grid*(i-1):grid*i,3);
    v = UVP(1 + grid*(i-1):grid*i,4);
    p = UVP(1 + grid*(i-1):grid*i,5);
    Xmed = reshape(x,Nx,Ny);
    Ymed = reshape(y,Nx,Ny);
    Umed = reshape(u,Nx,Ny);
    Vmed = reshape(v,Nx,Ny);
    Pmed = reshape(p,Nx,Ny);
    
end
%% fine grid
load residual256x96.dat;
load UVP256x96.dat;
UVP = UVP256x96;
resFine = residual256x96;
Nx = 256;
Ny = 96;

grid = Nx*Ny;
frames = length(UVP(:,1))/(grid);

x = zeros(Nx,Ny,frames);
y = zeros(Nx,Ny,frames);
u = zeros(Nx,Ny,frames);
v = zeros(Nx,Ny,frames);
p = zeros(Nx,Ny,frames);

for i = 1:frames
    % resphape UVP into frames
    x = UVP(1 + grid*(i-1):grid*i,1);
    y = UVP(1 + grid*(i-1):grid*i,2);
    u = UVP(1 + grid*(i-1):grid*i,3);
    v = UVP(1 + grid*(i-1):grid*i,4);
    p = UVP(1 + grid*(i-1):grid*i,5);
    Xfine = reshape(x,Nx,Ny);
    Yfine = reshape(y,Nx,Ny);
    Ufine = reshape(u,Nx,Ny);
    Vfine = reshape(v,Nx,Ny);
    Pfine = reshape(p,Nx,Ny);
%     pcolor(Xfine,Yfine,Pfine); colorbar; shading interp; hold on
%     drawnow
%     pause(.5)
end


%% steady state plots
figure()
pcolor(Xfine,Yfine,Ufine); colorbar; shading interp; colormap jet
title('U');
figure()
pcolor(Xfine,Yfine,Vfine); colorbar; shading interp; colormap jet
title('V');
figure()
pcolor(Xfine,Yfine,Pfine); colorbar; shading interp; colormap jet
title('Pressure');

%% plot residual vs time
figure()
semilogy(resFine(:,1),resFine(:,4), resMed(:,1),resMed(:,4), resCoarse(:,1),resCoarse(:,4))
legend('Fine','Med','Coarse')
xlabel('time')
ylabel('residual')

%% plot numerical solution
figure()
plot(Yfine(end,:))
title('Y profile')
figure()
plot(Ufine(end,:),linspace(0,1,length(Ufine(end,:))))
title('U profile')
figure()
plot(Vfine(end,:),linspace(0,1,length(Vfine(end,:))))
title('V profile')
% plot(Yfine(end,:),Ufine(end,:),'b')
% hold on
% plot(Yfine(end,:),Vfine(end,:),'g')
% plot(Ymed(end,:),Umed(end,:),'b--')
% plot(Ymed(end,:),Vmed(end,:),'g--')
% plot(Ycoarse(end,:),Ucoarse(end,:),'b-.')
% plot(Ycoarse(end,:),Vcoarse(end,:),'g-.')
% legend('u-fine','v-fine','u-med','v-med','u-coarse','v-coarse')
%% plot skin friction coeff

% find du/dy at y=0
% fine grid
dy_fine = Yfine(:,2)-Yfine(:,1);
dy_fine = dy_fine(1,1);
Uprime_fine = (Ufine(:,2) - Ufine(:,1))./dy_fine;
% medium grid
dy_med = Ymed(:,2)-Ymed(:,1);
dy_med = dy_med(1,1);
Uprime_med = (Umed(:,2) - Umed(:,1))./dy_med;
% coarse grid
dy_coarse = Ycoarse(:,2)-Ycoarse(:,1);
dy_coarse = dy_coarse(1,1);
Uprime_coarse = (Ucoarse(:,2) - Ucoarse(:,1))./dy_coarse;

nu = 1/5000;
Rex = linspace(.25,1,1000);
Rex_fine = linspace(0,1,length(Uprime_fine));
Rex_med = linspace(0,1,length(Uprime_med));
Rex_coarse = linspace(0,1,length(Uprime_coarse));
Cf_star = 0.664*Rex.^(-1/2);
Cf_fine = 2*nu.*Uprime_fine;
Cf_med = 2*nu.*Uprime_med;
Cf_coarse = 2*nu.*Uprime_coarse;


plot(Rex,Cf_star, Rex_fine,Cf_fine, Rex_med,Cf_med, Rex_coarse,Cf_coarse);
title('Skin friction coefficients')
legend('Blasius','fine','med','coarse')

%% table of skin friction error

% find errors
% fine
Rex = linspace(.25,1,3/4*length(Cf_fine));
Cf_star = 0.664*(3/4)*Rex.^(-1/2); % use correct grid
Cferr_fine = abs(Cf_fine(length(Cf_fine)/4+1:end) - Cf_star')./Cf_star';
% med
Rex = linspace(.25,1,3/4*length(Cf_med));
Cf_star = 0.664*(3/4)*Rex.^(-1/2); % use correct grid
Cferr_med = abs(Cf_med(length(Cf_med)/4+1:end) - Cf_star')./Cf_star';
% coarse
Rex = linspace(.25,1,3/4*length(Cf_coarse));
Cf_star = 0.664*(3/4)*Rex.^(-1/2); % use correct grid
Cferr_coarse = abs(Cf_coarse(length(Cf_coarse)/4+1:end) - Cf_star')./Cf_star';

% make table
fprintf('   Skin friction coefficient errors normalized with Cf_blasius \n')
fprintf('      Fine          Med          Coarse  \n')
for i = 1:length(Cferr_fine)
    if i <= length(Cferr_coarse)
        fprintf('%8.7e   %8.7e   %8.7e\n',Cferr_fine(i),Cferr_med(i),Cferr_coarse(i))
    elseif i <= length(Cferr_med)
        fprintf('%8.7e   %8.7e   \n',Cferr_fine(i),Cferr_med(i))
    else
        fprintf('%8.7e   \n',Cferr_fine(i))
    end
    
    
end
  

