DATA = UVP.dat residual.dat
OBJECTS = projection_method.o
.PHONY: clean cleanall

coarseGrid: UVP.dat residual.dat
	cp UVP.dat UVP64x24.dat
	cp residual.dat residual64x24.dat

mediumGrid: UVP.dat residual.dat
	cp UVP.dat UVP128x48.dat
	cp residual.dat residual128x48

fineGrid: UVP.dat residual.dat
	cp UVP.dat UVP256x96.dat
	cp residual.dat residual256x96.dat

UVP.dat: proj_method.exe
	./proj_method.exe

proj_method.exe: $(OBJECTS) 
	gfortran $(OBJECTS) -o proj_method.exe	

%.o: %.f90
	gfortran -c $<
clean:
	rm -f $(DATA) proj_method.exe *.o

cleanall:
	rm -f *.dat proj_method.exe *.o 