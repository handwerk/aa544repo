%% plot for aa544 homework 4
%% clear data
clear all;
close all;
clc;
%% set plot defaults
set(0,'DefaultAxesFontSize',18);
set(0,'DefaultLineLineWidth',2);

runCoarse = 1;
runMed = 1;
runFine = 1;
%% coarse grid
if runCoarse
    load resCoarse.dat;
    load uvpCoarse.dat;
    UVPc = uvpCoarse;
    Nxc = 64;
    Nyc = 24;
    
    grid = Nxc*Nyc;
    frames = length(UVPc(:,1))/(grid);
    
    x = zeros(Nxc,Nyc,frames);
    y = zeros(Nxc,Nyc,frames);
    u = zeros(Nxc,Nyc,frames);
    v = zeros(Nxc,Nyc,frames);
    p = zeros(Nxc,Nyc,frames);
    Cf = zeros(Nxc,Nyc,frames);
    
    for i = 1:frames
        % resphape UVP into frames
        x = UVPc(1 + grid*(i-1):grid*i,1);
        y = UVPc(1 + grid*(i-1):grid*i,2);
        u = UVPc(1 + grid*(i-1):grid*i,3);
        v = UVPc(1 + grid*(i-1):grid*i,4);
        p = UVPc(1 + grid*(i-1):grid*i,5);
        Cf = UVPc(1 + grid*(i-1):grid*i,6);
        
        Xcoarse = reshape(x,Nxc,Nyc);
        Ycoarse = reshape(y,Nxc,Nyc);
        Ucoarse = reshape(u,Nxc,Nyc);
        Vcoarse = reshape(v,Nxc,Nyc);
        Pcoarse = reshape(p,Nxc,Nyc);
        Cfcoarse = reshape(Cf,Nxc,Nyc);
        %     pcolor(Xcoarse,Ycoarse,Pcoarse); colorbar; shading interp; hold on
        %     drawnow
        %     pause(.5)
    end
end
%% fine grid
if runFine
    load resFine.dat;
    load uvpFine.dat;
    UVPf = uvpFine;
    Nxf = 256;
    Nyf = 96;
    
    grid = Nxf*Nyf;
    frames = length(UVPf(:,1))/(grid);
    
    x = zeros(Nxf,Nyf,frames);
    y = zeros(Nxf,Nyf,frames);
    u = zeros(Nxf,Nyf,frames);
    v = zeros(Nxf,Nyf,frames);
    p = zeros(Nxf,Nyf,frames);
    Cf = zeros(Nxf,Nyf,frames);
    
    for i = 1:frames
        % resphape UVP into frames
        x = UVPf(1 + grid*(i-1):grid*i,1);
        y = UVPf(1 + grid*(i-1):grid*i,2);
        u = UVPf(1 + grid*(i-1):grid*i,3);
        v = UVPf(1 + grid*(i-1):grid*i,4);
        p = UVPf(1 + grid*(i-1):grid*i,5);
        Cf = UVPf(1 + grid*(i-1):grid*i,6);
        Xfine = reshape(x,Nxf,Nyf);
        Yfine = reshape(y,Nxf,Nyf);
        Ufine = reshape(u,Nxf,Nyf);
        Vfine = reshape(v,Nxf,Nyf);
        Pfine = reshape(p,Nxf,Nyf);
        Cffine = reshape(Cf,Nxf,Nyf);
        %     pcolor(Xfine,Yfine,Pfine); colorbar; shading interp; hold on
        %     drawnow
        %     pause(.5)
    end
end

%% medium grid
if runMed
    load resMed.dat;
    load uvpMed.dat;
    UVPm = uvpMed;
    Nxm = 96;
    Nym = 36;
    
    grid = Nxm*Nym;
    frames = length(UVPm(:,1))/(grid);
    
    x = zeros(Nxm,Nym,frames);
    y = zeros(Nxm,Nym,frames);
    u = zeros(Nxm,Nym,frames);
    v = zeros(Nxm,Nym,frames);
    p = zeros(Nxm,Nym,frames);
    Cf = zeros(Nxm,Nym,frames);
    
    for i = 1:frames
        % resphape UVP into frames
        x = UVPm(1 + grid*(i-1):grid*i,1);
        y = UVPm(1 + grid*(i-1):grid*i,2);
        u = UVPm(1 + grid*(i-1):grid*i,3);
        v = UVPm(1 + grid*(i-1):grid*i,4);
        p = UVPm(1 + grid*(i-1):grid*i,5);
        Cf = UVPm(1 + grid*(i-1):grid*i,6);
        Xmed = reshape(x,Nxm,Nym);
        Ymed = reshape(y,Nxm,Nym);
        Umed = reshape(u,Nxm,Nym);
        Vmed = reshape(v,Nxm,Nym);
        Pmed = reshape(p,Nxm,Nym);
        Cfmed = reshape(Cf,Nxm,Nym);
        %     pcolor(Xmed,Ymed,Pmed); colorbar; shading interp; hold on
        %     drawnow
        %     pause(.5)
    end
end
%% steady state plots
%coarse
if runCoarse
    figure()
    pcolor(Xcoarse,Ycoarse,Ucoarse); colorbar; colormap jet; shading interp; axis square
    title('Ucoarse');
    figure()
    pcolor(Xcoarse,Ycoarse,Vcoarse); colorbar; colormap jet; shading interp; axis square
    title('Vcoarse');
    figure()
    pcolor(Xcoarse,Ycoarse,Pcoarse); colorbar; colormap jet; shading interp; axis square
    title('Pcoarse');
end
%medium
if runMed
    figure()
    pcolor(Xmed,Ymed,Umed); colorbar; colormap jet; shading interp; axis square
    title('Umed');
    figure()
    pcolor(Xmed,Ymed,Vmed); colorbar; colormap jet; shading interp; axis square
    title('Vmed');
    figure()
    pcolor(Xmed,Ymed,Pmed); colorbar; colormap jet; shading interp; axis square
    title('Pmed');
end
%fine
if runFine
    figure()
    pcolor(Xfine,Yfine,Ufine); colorbar; colormap jet; shading interp; axis square
    title('Ufine');
    figure()
    pcolor(Xfine,Yfine,Vfine); colorbar; colormap jet; shading interp; axis square
    title('Vfine');
    figure()
    pcolor(Xfine,Yfine,Pfine); colorbar; colormap jet; shading interp; axis square
    title('Pfine');
end

%% quiver/streamline
%
% [sx1, sy1] = meshgrid(1, 1:2:Nyf); % inlet
%
% figure()
% streamline(Ufine',Vfine',sx1,sy1,[.1,2000]); axis square
% title('Streamlines')

%% plot residual vs time

if runCoarse && runMed && runFine
    figure()
    semilogy(resCoarse(:,1),resCoarse(:,4), resMed(:,1),resMed(:,4), resFine(:,1),resFine(:,4))
    % semilogy(resFine(:,1),resFine(:,4));
    legend('Coarse','Med','Fine')
    xlabel('time')
    ylabel('residual')
end
%% friction coefficient
Cf_star_coarse = (0.058.*(Xcoarse-2.5).^(-1/5))./3;
Cf_star_med = (0.058.*(Xmed-2.5).^(-1/5))./3;
Cf_star_fine = (0.058.*(Xfine-2.5).^(-1/5))./3;
figure()
plot(Xfine, Cf_star_fine,'k')
hold on
if runCoarse
    plot(Xcoarse(Nxc/4:end,end),Cfcoarse(Nxc/4:end,end),'b')
    hold on
end
if runMed
    plot(Xmed(Nxm/4:end,end),Cfmed(Nxm/4:end,:),'g')
    hold on
end
if runFine
    plot(Xfine(Nxf/4:end,end),Cffine(Nxf/4:end,end),'r')
    hold on
end

if runCoarse && runMed && runFine % make table of errors
    diffCoarse = sum(abs(Cf_star_coarse(Nxc/4+1:end,end) - ...
        Cfcoarse(Nxc/4+1:end,end)))/length(Cfcoarse(Nxc/4+1:end,end));
    diffMed = sum(abs(Cf_star_med(Nxm/4+1:end,end) - ...
        Cfmed(Nxm/4+1:end,end)))/length(Cfmed(Nxm/4+1:end,end));
    diffFine = sum(abs(Cf_star_fine(Nxf/4+1:end,end) - ...
        Cffine(Nxf/4+1:end,end)))/length(Cffine(Nxf/4+1:end,end));
    
    diffC = abs(Cf_star_coarse(Nxc/4+1:end,end) - Cfcoarse(Nxc/4+1:end,end));
    solnC = Cf_star_coarse(Nxc/4+1:end,end);
    perCoarse = sum(diffC/solnC)/length(solnC);
    diffM = abs(Cf_star_med(Nxm/4+1:end,end) - Cfmed(Nxm/4+1:end,end));
    solnM = Cf_star_med(Nxm/4+1:end,end);
    perMed = sum(diffM/solnM)/length(solnM);
    diffF = abs(Cf_star_fine(Nxf/4+1:end,end) - Cffine(Nxf/4+1:end,end));
    solnF = Cf_star_fine(Nxf/4+1:end,end);
    perFine = sum(diffF/solnF)/length(solnF);
    
    fprintf('%16s %16s %16s\n','grid','ave diff','percentage')
    fprintf('%16s %16.8f %16.8f\n','coarse',diffCoarse,perCoarse)
    fprintf('%16s %16.8f %16.8f\n','medium',diffMed,perMed)
    fprintf('%16s %16.8f %16.8f\n','fine',diffFine,perFine)
end

title('C_f')
%% velocity profiles
% if runCoarse
%     % coarse u
%     figure()
%     subplot(2,2,1)
%     plot(flipud(Ucoarse(Nxc/4,1:Nyc))); view(-90,90);axis ij;title('coarse x=1/4'); axis square
%     subplot(2,2,2)
%     plot(flipud(Ucoarse(Nxc/2,1:Nyc))); view(-90,90);axis ij;title('x=2/4'); axis square
%     subplot(2,2,3)
%     plot(flipud(Ucoarse(3*Nxc/4,1:Nyc))); view(-90,90);axis ij;title('x=3/4'); axis square
%     subplot(2,2,4)
%     plot(flipud(Ucoarse(Nxc,1:Nyc))); view(-90,90);axis ij;title('x=end'); axis square
%     
%     % coarse v
%     figure()
%     subplot(2,2,1)
%     plot(flipud(Vcoarse(Nxc/4,1:Nyc))); view(-90,90);title('V coarse'); axis square
%     subplot(2,2,2)
%     plot(flipud(Vcoarse(Nxc/2,1:Nyc))); view(-90,90);title('y=2/4'); axis square
%     subplot(2,2,3)
%     plot(flipud(Vcoarse(3*Nxc/4,1:Nyc))); view(-90,90);title('y=3/4'); axis square
%     subplot(2,2,4)
%     plot(flipud(Vcoarse(Nxc,1:Nyc))); view(-90,90);title('y=end'); axis square
% end
% 
% if runFine
%     % fine u
%     figure()
%     subplot(2,2,1)
%     plot(flipud(Ufine(Nxf/4,1:Nyf))); view(-90,90);axis ij;title('fine x=1/4'); axis square
%     subplot(2,2,2)
%     plot(flipud(Ufine(Nxf/2,1:Nyf))); view(-90,90);axis ij;title('x=2/4'); axis square
%     subplot(2,2,3)
%     plot(flipud(Ufine(3*Nxf/4,1:Nyf))); view(-90,90);axis ij;title('x=3/4'); axis square
%     subplot(2,2,4)
%     plot(flipud(Ufine(Nxf,1:Nyf))); view(-90,90);axis ij;title('x=end'); axis square
%     
%     % fine v
%     figure()
%     subplot(2,2,1)
%     plot(flipud(Vfine(Nxf/4,1:Nyf))); view(-90,90);title('V fine'); axis square
%     subplot(2,2,2)
%     plot(flipud(Vfine(Nxf/2,1:Nyf))); view(-90,90);title('y=2/4'); axis square
%     subplot(2,2,3)
%     plot(flipud(Vfine(3*Nxf/4,1:Nyf))); view(-90,90);title('y=3/4'); axis square
%     subplot(2,2,4)
%     plot(flipud(Vfine(Nxf,1:Nyf))); view(-90,90);title('y=end'); axis square
% end